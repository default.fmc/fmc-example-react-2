// api
import { chatApi } from '../../api/chat';
// types
import { errorType } from '../../types/acions/app';
import {
  changeStatusInListReturnType,
  changeStatusReturnType,
  chatActionType,
  chatImageRequestType,
  chatInfoClearReturnType,
  chatInfoReturnType,
  chatListItemResponseType,
  chatListItemType,
  messageType,
  chatMessageRequestType,
  chatMessagesListItemType,
  chatNotificationReturnType,
  chatRecomendationsItemRequestType,
  closeChatReturnType,
  createEventSourceReturnType,
  getChatInfoReturnType,
  getChatsListActionRequestType,
  getChatsListByParamsClearReturnType,
  getChatsListByParamsLoaderReturnType,
  getChatsListByParamsReturnType,
  getChatsListReturnType,
  getChatsListUpdateReturnType,
  getMessagesListByParamsClearReturnType,
  getMessagesListByParamsLoaderReturnType,
  getMessagesListByParamsReturnType,
  getMessagesListReturnType,
  getRecomendationResultReturnType,
  setDownloadBtnControllerType,
  joinChatReturnType,
  pushMessageToListReturnType,
  recomendationPushToChatReturnType,
  recomendationResultReturnType,
  recomendationToChatLoadingReturnType,
  sendImageMessageReturnType,
  sendMessageReturnType,
} from '../../types/acions/chat';
import { searchedProductType } from '../../types/acions/product';

const chatAction: chatActionType = {
  createEventSource(es: any): createEventSourceReturnType {
    return {
      type: 'CHATS_ES',
      payload: {
        es,
      },
    };
  },
  getChatsList(data: getChatsListActionRequestType): getChatsListReturnType {
    return {
      type: 'GET_CHATS_LIST',
      data,
    };
  },
  getChatsListUpdate(data: getChatsListActionRequestType): getChatsListUpdateReturnType {
    return {
      type: 'GET_CHATS_LIST_UPDATE',
      data,
    };
  },
  getChatsListByParams(response: Array<chatListItemType>): getChatsListByParamsReturnType {
    return {
      type: 'CHATS_LIST',
      payload: {
        chatsList: response,
      },
    };
  },
  getChatsListByParamsClear(): getChatsListByParamsClearReturnType {
    return {
      type: 'CHATS_LIST_CLEAR',
      payload: {
        chatsList: [],
      },
    };
  },
  getChatsListByParamsLoader(status: boolean, progress: number): getChatsListByParamsLoaderReturnType {
    const loading = {
      status: status,
      progress: progress,
    };
    return {
      type: 'CHATS_LIST_LOADING',
      payload: {
        listLoading: { ...loading },
      },
    };
  },
  getMessagesList(data: number, userId: number): getMessagesListReturnType {
    return {
      type: 'GET_MESSAGES_LIST',
      data,
      userId,
    };
  },
  getMessagesListByParams(response: Array<chatMessagesListItemType>): getMessagesListByParamsReturnType {
    return {
      type: 'MESSAGES_LIST',
      payload: {
        messagesList: response,
      },
    };
  },
  getMessagesListByParamsClear(): getMessagesListByParamsClearReturnType {
    return {
      type: 'MESSAGES_LIST',
      payload: {
        messagesList: [],
      },
    };
  },
  getMessagesListByParamsLoader(status: boolean, progress: number): getMessagesListByParamsLoaderReturnType {
    const loading = {
      status: status,
      progress: progress,
    };
    return {
      type: 'MESSAGES_LIST_LOADING',
      payload: {
        messagesLoading: { ...loading },
      },
    };
  },
  pushMessageToList(
    message: chatMessagesListItemType,
    list: Array<chatMessagesListItemType>,
  ): pushMessageToListReturnType {
    return {
      type: 'MESSAGES_LIST',
      payload: {
        messagesList: [...list, message],
      },
    };
  },
  changeStatusInList(id: number, status: string, list: any): changeStatusInListReturnType {
    return {
      type: 'MESSAGES_LIST',
      payload: {
        messagesList: list.map((item) => {
          if (item.id === id) {
            return (item.globalStatus = status);
          }
        }),
      },
    };
  },
  getChatInfo(id: number): getChatInfoReturnType {
    return {
      type: 'GET_CHAT_INFO',
      id,
    };
  },
  chatInfo(data: chatListItemResponseType): chatInfoReturnType {
    return {
      type: 'ACTIVE_CHAT_INFO',
      payload: {
        activeChatInfo: data,
      },
    };
  },
  chatInfoClear(): chatInfoClearReturnType {
    return {
      type: 'ACTIVE_CHAT_INFO',
      payload: {
        activeChatInfo: [],
      },
    };
  },
  changeStatus(object: any): changeStatusReturnType {
    return {
      type: 'ACTIVE_CHAT_OBJECT',
      payload: {
        activeChatObject: object,
      },
    };
  },
  sendMessage(data: chatMessageRequestType): sendMessageReturnType {
    return {
      type: 'SEND_MESSAGE',
      data,
    };
  },
  sendImageMessage(images: chatImageRequestType): sendImageMessageReturnType {
    return {
      type: 'SEND_IMAGE_MESSAGE',
      images,
    };
  },
  joinChat(id: number): joinChatReturnType {
    return {
      type: 'JOIN_CHAT',
      id,
    };
  },
  closeChat(id: number, data: getChatsListActionRequestType): closeChatReturnType {
    return {
      type: 'CLOSE_CHAT',
      id,
      data,
    };
  },
  getRecomendationResult(data: string): getRecomendationResultReturnType {
    return {
      type: 'GET_RECOMENDATION_RESULT',
      data,
    };
  },
  recomendationResult(data: Array<searchedProductType>): recomendationResultReturnType {
    return {
      type: 'RECOMENDATION_RESULT',
      payload: {
        recomendationResult: data,
      },
    };
  },
  recomendationPushToChat(data: chatRecomendationsItemRequestType): recomendationPushToChatReturnType {
    return {
      type: 'RECOMENDATION_PUSH_TO_CHAT',
      data,
    };
  },
  recomendationToChatLoading(status: boolean, progress: number): recomendationToChatLoadingReturnType {
    const loading = {
      status: status,
      progress: progress,
    };
    return {
      type: 'RECOMENDATION_RESULT_LOADING',
      payload: {
        recomendationResultLoading: { ...loading },
      },
    };
  },
  async dowloadRecipesFromList(uuid: number, setDownloadBtnController: setDownloadBtnControllerType, item: messageType) {
    try {
      const response = await chatApi.download(uuid);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', item.recipeImage.originalFilename);
      document.body.appendChild(link);
      link.click();
      setDownloadBtnController(false);
    } catch (e) {
      console.log(e);
      setDownloadBtnController(false);
    }
  },
  chatNotification(status: boolean, error: errorType, type: string): chatNotificationReturnType {
    return {
      type: 'APP_NOTIFICATION',
      payload: {
        notification: {
          status: status,
          type: type,
          message: {
            header: error.header,
            body: error.message,
          },
        },
      },
    };
  },
};

export default chatAction;

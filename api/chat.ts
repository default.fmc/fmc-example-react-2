import axios from 'axios';
import { CONSTS } from '../configs/consts';
// types
import {
  chatImageType,
  chatListItemResponseType,
  chatListResponseType,
  chatMessageRequestType,
  chatMessagesListResponseType,
  chatRecomendationsResponseType,
  chatRecomendationsItemRequestType,
} from '../types/acions/chat';
import { chatApiType } from '../types/api/chat';

export const chatApi: chatApiType = {
  getChatsListByParams: async (): Promise<{ data: chatListResponseType }> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chats?all=true`,
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  getMessagesListByParams: async (id: number): Promise<{ data: chatMessagesListResponseType }> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${id}/messages`,
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  sendMessage: async (data: chatMessageRequestType): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${data.id}/message`,
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      data: {
        text: data.text,
      },
    });
  },
  sendImageMessage: async (data: chatImageType): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${data.id}/application/${data.uuid}`,
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  sendPdfMessage: async (data: any): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${data.id}/recipe/${data.recipe_id}`,
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  joinChat: async (id: number): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/user/join/${id}`,
      method: 'PATCH',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  closeChat: async (id: number): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}user/chat/${id}/close`,
      method: 'PATCH',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  download: async (url: string): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}recipe/file/${url}`,
      method: 'GET',
      responseType: 'arraybuffer',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  getChatInfo: async (id: number): Promise<{ data: chatListItemResponseType }> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${id}`,
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
  getRecomendationResult: async (data: string): Promise<{ data: chatRecomendationsResponseType }> => {
    return await axios({
      url: `${CONSTS.API.URL}public/products/search?name=${data}&page=1&per_page=50`,
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
    });
  },
  recomendationPushToChat: async (data: chatRecomendationsItemRequestType): Promise<any> => {
    return await axios({
      url: `${CONSTS.API.URL}chat/chat/${data.chat_id}/global-product/${data.product_id}`,
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
  },
};

const initialState = {
  es: {},
  chatsList: [],
  messagesList: [],
  activeChatObject: {},
  activeChatInfo: {},
  listLoading: {
    status: false,
    progress: 0,
  },
  messagesLoading: {
    status: false,
    progress: 0,
  },
  recomendationResultLoading: {
    status: false,
    progress: 0,
  },
  recomendationsList: [
    { id: 1, name: 'No-spa', link: '/link', period: 2 },
    { id: 2, name: 'Analhin', link: '/link', period: 1 },
    { id: 3, name: 'Citramone', link: '/link', period: 2 },
    { id: 4, name: 'Sweet Syrup', link: '/link', period: 3 },
  ],
  recomendationResult: [],
};

const chat = (state = initialState, action) => {
  switch (action.type) {
    case 'CHATS_ES':
      return Object.assign({}, state, {
        es: action.payload.es,
      });
    case 'CHATS_LIST':
      return Object.assign({}, state, {
        chatsList: action.payload.chatsList,
      });
    case 'CHATS_LIST_LOADING':
      return Object.assign({}, state, {
        listLoading: action.payload.listLoading,
      });
    case 'CHATS_LIST_ERROR':
      return Object.assign({}, state, {
        error: action.payload.error,
      });
    case 'ACTIVE_CHAT_OBJECT':
      return Object.assign({}, state, {
        activeChatObject: action.payload.activeChatObject,
      });
    case 'ACTIVE_CHAT_INFO':
      return Object.assign({}, state, {
        activeChatInfo: action.payload.activeChatInfo,
      });
    case 'CHANGE_STATUS':
      return Object.assign({}, state, {
        chatsList: action.payload.chatsList,
      });
    case 'MESSAGES_LIST':
      return Object.assign({}, state, {
        messagesList: action.payload.messagesList,
      });
    case 'MESSAGES_LIST_LOADING':
      return Object.assign({}, state, {
        messagesLoading: action.payload.messagesLoading,
      });
    case 'MESSAGES_LIST_ERROR':
      return Object.assign({}, state, {
        error: action.payload.error,
      });
    case 'RECOMENDATION_RESULT':
      return Object.assign({}, state, {
        recomendationResult: action.payload.recomendationResult,
      });
    case 'RECOMENDATION_RESULT_LOADING':
      return Object.assign({}, state, {
        recomendationResultLoading: action.payload.recomendationResultLoading,
      });
    default:
      return state;
  }
};

export default chat;

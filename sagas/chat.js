import { put, call, all } from 'redux-saga/effects';
// api
import { chatApi } from '../../api/chat';
// actions
import userAction from '../actions/user';
import chatAction from '../actions/chat';
// helpers
import { buildErrorObject, buildSuccessObject } from './helpers';
// mapping
import { mapping } from '../mapping';

export function* sagaGetChatInfo(action) {
  try {
    yield put(chatAction.chatInfoClear());
    const response = yield call(chatApi.getChatInfo, action.id);
    yield put(chatAction.chatInfo(response.data.data.item));
  } catch (e) {
    console.log('e', e);
  }
}

export function* sagaGetChatsList(action) {
  try {
    yield put(userAction.userNotification(false, {}, ''));
    yield put(chatAction.getChatsListByParamsLoader(false, 0));
    yield put(chatAction.getChatsListByParamsClear());
    yield put(chatAction.getChatsListByParamsLoader(true, 90));
    const responseChatsList = yield call(chatApi.getChatsListByParams, action.data);
    yield put(chatAction.getChatsListByParamsLoader(false, 100));
    yield put(chatAction.getChatsListByParams(mapping.buildChatList(responseChatsList.data.data.items)));
  } catch (e) {
    yield put(chatAction.getChatsListByParamsLoader(true, 100));
    yield put(chatAction.getChatsListByParamsLoader(false, 100));
    yield put(userAction.userNotification(true, buildErrorObject(e), 'error'));
  }
}

export function* sagaGetChatsListUpdate(action) {
  try {
    const responseChatsList = yield call(chatApi.getChatsListByParams, action.data);
    yield put(chatAction.getChatsListByParams(mapping.buildChatList(responseChatsList.data.data.items)));
  } catch (e) {}
}

export function* sagaGetMessagesList(action) {
  try {
    yield put(userAction.userNotification(false, {}, ''));
    yield put(chatAction.getMessagesListByParamsLoader(false, 0));
    yield put(chatAction.getMessagesListByParamsClear());
    yield put(chatAction.getMessagesListByParamsLoader(true, 90));
    const responseMessagesList = yield call(chatApi.getMessagesListByParams, action.data);
    yield put(chatAction.getMessagesListByParamsLoader(false, 100));
    yield put(
      chatAction.getMessagesListByParams(
        mapping.buildMessagesList(responseMessagesList.data.data.items, action.userId),
      ),
    );
  } catch (e) {
    yield put(chatAction.getMessagesListByParamsLoader(true, 100));
    yield put(chatAction.getMessagesListByParamsLoader(false, 100));
    yield put(userAction.userNotification(true, buildErrorObject(e), 'error'));
  }
}

export function* sagaSendMessage(action) {
  try {
    yield call(chatApi.sendMessage, action.data);
  } catch (e) {
    yield put(userAction.userNotification(true, { header: 'Error', message: 'Message is not sent' }, 'error'));
  }
}

export function* sagaSendImageMessage(action) {
  try {
    yield all(
      action.images.newFiles.map((item) => {
        let data = { id: action.images.id, uuid: item.uuid };
        return call(chatApi.sendImageMessage, data);
      }),
    );
  } catch (e) {
    console.log('e', e);
  }
}

export function* sagaJoinChat(action) {
  try {
    yield put(userAction.userNotification(false, {}, ''));
    const responseJoinChat = yield call(chatApi.joinChat, action.id);
    yield put(userAction.userNotification(true, buildSuccessObject(responseJoinChat), 'success'));
  } catch (e) {
    yield put(userAction.userNotification(true, buildErrorObject(e), 'error'));
  }
}

export function* sagaCloseChat(action) {
  try {
    yield put(userAction.userNotification(false, {}, ''));
    const responseCloseChat = yield call(chatApi.closeChat, action.id);
    yield put(
      userAction.userNotification(
        true,
        { header: 'Success', message: 'You have sent a request to close the chat' },
        'success',
      ),
    );
    yield put(chatAction.getChatsListUpdate(action.data));
  } catch (e) {
    yield put(userAction.userNotification(true, buildErrorObject(e), 'error'));
  }
}

export function* sagaGetRecomendationResult(action) {
  try {
    yield put(chatAction.recomendationToChatLoading(false, 0));

    yield put(chatAction.recomendationResult([]));
    yield put(chatAction.recomendationToChatLoading(true, 100));
    const response = yield call(chatApi.getRecomendationResult, action.data);
    yield put(chatAction.recomendationResult(response.data.data.items));
    yield put(chatAction.recomendationToChatLoading(false, 0));
  } catch (e) {
    yield put(chatAction.recomendationToChatLoading(false, 0));
    console.log('e', e);
  }
}

export function* sagaRecomendationPushToChat(action) {
  try {
    const response = yield call(chatApi.recomendationPushToChat, action.data);
  } catch (e) {
    console.log('e', e);
  }
}

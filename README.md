Project name 'Example'.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project has atomic design [https://bradfrost.com/blog/post/atomic-web-design/](atomic-web-design)
With small customisation.

#### root
```
public (text, index.html)
src
- action
- api
- assets
- components (atomic design)
- configs
- pages
- redux (actions, dispachTypes, reducers, sagas)
- tools
- types (typescripts)
- index.tsx
```
#### example page
```
layout
 - LayoutComponent - high order component(render controller)
page
 - PageComponent
 - TemplateComponent
    - OrganismComponet`s
        - AtomsComponent`s
        - MoleculesComponet`s
 - BlockCompoents ( group with Atoms, Molecules and other block`s)
```
#### config`s
objects for build table or forms for unother pages
```
- configs/
    - index (config for main table)
    - [name].js (config for other tables on this page)
    - helpers.js (config for object form on this page)

    - consts.js (config with seting connect to api)
```
# Version`s node
node - v12.18.4
npm  - v6.14.6
yarn - v1.22.5

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `main dependencies
For work with api - `axios`
For work with forms - `react-hook-form`
For work with tables - `react-bootstrap-table-next`
For work with merkure - `event-source-polyfill`
For work with scroll - `react-scroll`
For work with js - `lodash` 

Use components from - `antd, semantic ui`

```
```
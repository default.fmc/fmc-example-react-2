import React, { FC } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { Route } from 'react-router-dom';
// components
// import Layout from './Layout';
import UserLayout from './UserLayout';
import LoginLayout from './LoginLayout';
import Home from '../../pages/home';
// pages
import pageStatisctic from '../../pages/statistic';
import pageUserProfile from '../../pages/userProfile';
import pageSelfProfile from '../../pages/selfProfile';
import pageAdmin from '../../pages/administation/admin';
import pageEnterpriseAdmins from '../../pages/administation/enterpriseAdmins';
import pageCourier from '../../pages/administation/courier';
import pageDoctor from '../../pages/administation/doctor';
import pagePharmacists from '../../pages/administation/pharmacists';
import pageEnterpriseManagers from '../../pages/administation/enterpriseManagers';
import pageManager from '../../pages/administation/manager';
import pageMedicine from '../../pages/medicine';
import pageOrders from '../../pages/orders';
import pageEditOrder from '../../pages/orders/editOrder';
import pageEditProduct from '../../pages/medicine/editProduct';
import pageCreateProduct from '../../pages/medicine/createProduct';
import pageCustomers from '../../pages/customers';
import pageEditCustomer from '../../pages/customers/editCustomer';
import pageRecipes from '../../pages/recipes';
import pageEditRecipe from '../../pages/recipes/editRecipe';
import pagePharmacies from '../../pages/pharmacies';
import CreateProductPharmacyPage from '../../pages/pharmacies/createProduct';
import EditProductPharmacyPage from '../../pages/pharmacies/editProduct';
import pageEditPharmacy from '../../pages/pharmacies/editPharmacy';
import pageChat from '../../pages/chat';
import Authorization from '../../pages/authorization';
import ForgotPassword from '../../pages/forgotPassword';
import ForgotPasswordLink from '../../pages/forgotPasswordLink';
import ConfirmResetPassword from '../../pages/confirmResetPassword';
import NothfoundPage from '../../pages/nothfoundPage';
// styles
import 'semantic-ui-css/semantic.min.css';
import '../../assets/main.scss';

const App: FC = () => {
  return (
    <Router>
      <Switch>
        <UserLayout exact path="/profile" component={pageSelfProfile} />
        <UserLayout exact path="/" component={pageStatisctic} />
        <UserLayout exact path="/statistics" component={pageStatisctic} />
        <UserLayout exact path="/administration" component={Home} />
        <UserLayout exact path="/administration/admin" component={pageAdmin} />
        <UserLayout exact path="/administration/admin/:id" component={pageUserProfile} type="Admin" />
        <UserLayout exact path="/administration/enterprise-admins" component={pageEnterpriseAdmins} />
        <UserLayout
          exact
          path="/administration/enterprise-admin/:id"
          component={pageUserProfile}
          type="Enterprise Admin"
        />
        <UserLayout exact path="/administration/courier" component={pageCourier} />
        <UserLayout exact path="/administration/courier/:id" component={pageUserProfile} type="Courier" />
        <UserLayout exact path="/administration/doctor" component={pageDoctor} />
        <UserLayout exact path="/administration/doctor/:id" component={pageUserProfile} type="Doctor" />
        <UserLayout exact path="/administration/pharmacists" component={pagePharmacists} />
        <UserLayout exact path="/administration/pharmacist/:id" component={pageUserProfile} type="Pharmacist" />

        <UserLayout exact path="/administration/enterprise-managers" component={pageEnterpriseManagers} />
        <UserLayout
          exact
          path="/administration/enterprise-manager/:id"
          component={pageUserProfile}
          type="Enterprise Manager"
        />
        <UserLayout exact path="/administration/manager" component={pageManager} />
        <UserLayout exact path="/administration/manager/:id" component={pageUserProfile} type="Manager" />
        <UserLayout exact path="/medicine" component={pageMedicine} />
        <UserLayout exact path="/medicine/product/:id/edit" component={pageEditProduct} />
        <UserLayout exact path="/medicine/product/create" component={pageCreateProduct} />
        <UserLayout exact path="/orders" component={pageOrders} />
        <UserLayout exact path="/customers" component={pageCustomers} />
        <UserLayout exact path="/customers/:id" component={pageEditCustomer} />
        <UserLayout exact path="/orders/edit/:id" component={pageEditOrder} />
        <UserLayout exact path="/recipes" component={pageRecipes} />
        <UserLayout exact path="/recipes/edit/:id" component={pageEditRecipe} />
        <UserLayout exact path="/pharmacies" component={pagePharmacies} />
        <UserLayout exact path="/pharmacies/:id" component={pageEditPharmacy} />
        <UserLayout exact path="/pharmacies/:id/product/create" component={CreateProductPharmacyPage} />
        <UserLayout exact path="/pharmacies/:id/product/:product_id/edit" component={EditProductPharmacyPage} />
        <UserLayout exact path="/chats" component={pageChat} />
        <LoginLayout exact path="/login" component={Authorization} />
        <Route exact path="/forgot_password" component={ForgotPassword} />
        <Route exact path="/forgot_password_link" component={ForgotPasswordLink} />
        <Route exact path="/forgot_password/confirm" component={ConfirmResetPassword} />
        <Route path="*" component={NothfoundPage} />
      </Switch>
    </Router>
  );
};

export default App;

import React, { FC, useState } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
// components
import InputField from '../../molecules/inputField';
// actions
import appAction from '../../../redux/actions/app';
// types
import { loadingType } from '../../../types/acions/app';
import { stateType } from '../../../types/acions/state';
// style
import './style.scss';

interface FormData {
  email: string;
  password: string;
}

const BoxFormLogin: FC = () => {

  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [sumbmitController, setSumbmitController] = useState<boolean>(false);

  const { register, handleSubmit, errors } = useForm<FormData>();

  const loading: loadingType = useSelector((state: stateType) => state.user.loading, shallowEqual)

  const dispatch = useDispatch();
  const history = useHistory();

  const callback = () => {
    history.push('/');
  };

  const inputHandleChange = () => {
    sumbmitController && setSumbmitController(false);
  };

  const onSubmit = (data: FormData) => {
    dispatch(appAction.login(data, callback));
    setSumbmitController(true);
  };

  const togglePasswordField = () => {
    setShowPassword(!showPassword);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="form form-auth">
      <InputField classes="input-field" icon={errors.email && 'icon-field-error'}>
        <input
          className={errors.email && 'error'}
          id="email"
          name="email"
          placeholder="E-Mail Address"
          onChange={inputHandleChange}
          ref={register({
            required: 'Enter your e-mail',
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: 'Email wrong',
            },
          })}
        />
        {errors.email && (
          <span role="alert" className="text-error">
            {errors.email.message}
          </span>
        )}
      </InputField>
      <InputField classes="input-field" icon="icon-field-show" iconAction={togglePasswordField}>
        <input
          className={errors.password && 'error'}
          id="password"
          name="password"
          placeholder="Password"
          onChange={inputHandleChange}
          ref={register({
            required: 'Required field.',
          })}
          type={showPassword ? 'input' : 'password'}
        />
        {errors.password && (
          <span role="alert" className="text-error">
            {errors.password.message}
          </span>
        )}
      </InputField>
      <div className="form-footer">
        <div />
        <Link to="/forgot_password">Forgot your password?</Link>
      </div>

      <button className="button btn-default" type="submit" disabled={loading.status || sumbmitController}>
        Log In
      </button>
    </form>
  );
};

export default BoxFormLogin;

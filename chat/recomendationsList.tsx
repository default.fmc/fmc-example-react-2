import React, { FC } from 'react';
import { useSelector } from 'react-redux';
// types
import { stateType } from '../../../types/acions/state';
import { recomendationsListType } from '../../../types/acions/chat';

const RecomendationsList: FC = () => {
  const recomendationsList: recomendationsListType = useSelector((state: stateType) => state.chat.recomendationsList)

  return (
    <div className="recomendations-list">
      <div className="recomendations-list-header">
        <div className="recomendations-list-header-num">
          <h2>Num</h2>
        </div>
        <div className="recomendations-list-header-medicine">
          <h2>Medecine</h2>
        </div>
        <div className="recomendations-list-header-period">
          <h2>Period</h2>
        </div>
      </div>
      {recomendationsList.map((item) => {
        return (
          <div className="recomendations-list-item" key={item.id}>
            <div className="recomendations-list-item-num">
              <div className="recomendations-list-item-num-container">
                <div className="icon icon-recomendations-delete" />
                {item.id}
              </div>
            </div>
            <div className="recomendations-list-item-medicine">{item.name}</div>
            <div className="recomendations-list-item-period">{item.period}</div>
          </div>
        );
      })}
    </div>
  );
};

export default RecomendationsList;

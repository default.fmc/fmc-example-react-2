import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import { EventSourcePolyfill } from 'event-source-polyfill/src/eventsource';
// actions
import chatAction from '../../../redux/actions/chat';
import { mapping } from '../../../redux/mapping';
// types
import { stateType } from '../../../types/acions/state';
import { chatListItemType, chatMessagesListItemType, messageType, messagesListType } from '../../../types/acions/chat';
import { loadingType } from '../../../types/acions/app';
// consts
import { CONSTS } from '../../../configs/consts';
// images
import Loader from '../../../assets/image/loaders/loading.gif';
// style
import './style.scss';

interface IChatsList {
  es: any;
  chatsList: Array<chatListItemType>;
  listLoading: loadingType;
  messagesLoading: loadingType;
  userId: number;
  activeChatObject: chatListItemType;
  userTopicName: string;
  messagesList: Array<chatMessagesListItemType>;
}

const ChatsList: FC = () => {
  const { es,
    chatsList,
    listLoading, 
    messagesLoading,
    userId,
    activeChatObject, 
    userTopicName, 
    messagesList }: IChatsList = useSelector((state: stateType) => state.chat)

  const dispatch = useDispatch();

  const data = {
    uuid: userId,
    page: 1,
    per_page: 100,
  };

  const mercureConnect = () => {
    const url = new URL(`${CONSTS.MERCURE.URL}.well-known/mercure`);
    url.searchParams.append('topic', `${userTopicName}`);

    const es = new EventSourcePolyfill(url, {});

    dispatch(chatAction.createEventSource(es));
  };

  if (!_.isEmpty(es)) {
    es.onmessage = (event) => {
      const obj = JSON.parse(event.data);
      dispatch(chatAction.getChatsListUpdate(data));
      if (activeChatObject.id === obj.body.item.chatId) {
        onmessage(obj, messagesList);
      }
      if (obj.type === 'change_status') {
        if (obj.body.item.id === activeChatObject.id) {
          return dispatch(chatAction.changeStatus(mapping.buildChatItem(obj.body.item)));
        }
      }
    };
  }

  const onmessage = (message: messageType, messagesList: messagesListType) => {
    let createMessage: messageType;
    switch (message.type) {
      case 'message':
        createMessage = {
          id: message.body.item.id,
          type: 'text',
          isCustomersMessage: message.body.item.ownerType === 'user' ? false : true,
          message: message.body.item.text,
          time: moment(message.body.item.updatedAt).format('HH:mm'),
        };
        return dispatch(chatAction.pushMessageToList(createMessage, messagesList));

      case 'application':
        createMessage = {
          id: message.body.item.id,
          type: 'file',
          isCustomersMessage: message.body.item.ownerType === 'user' ? false : true,
          message: message.body.item.file,
          time: moment(message.body.item.updatedAt).format('HH:mm'),
        };
        return dispatch(chatAction.pushMessageToList(createMessage, messagesList));

      case 'global_product':
        createMessage = {
          id: message.body.item.id,
          type: 'product',
          isCustomersMessage: message.body.item.ownerType === 'user' ? false : true,
          message: message.body.item.globalProductCard,
          time: moment(message.body.item.updatedAt).format('HH:mm'),
        };
        return dispatch(chatAction.pushMessageToList(createMessage, messagesList));
      case 'recipe':
        createMessage = {
          id: message.body.item.id,
          type: 'recipt',
          time: moment(message.body.item.createdAt).format('HH:mm'),
          recipeImage: message.body.item.recipeImage,
          isCustomersMessage: false,
        };
        return dispatch(chatAction.pushMessageToList(createMessage, messagesList));
    }
  };

  useEffect(() => {
    mercureConnect();
    dispatch(chatAction.getChatsList(data));
    return () => {
      dispatch(chatAction.getChatsListByParams([]));
      dispatch(chatAction.changeStatus({}));
      !_.isEmpty(es) && es.close();
    };
  }, []);

  const clickOnChatListItem = (item) => {
    if (!messagesLoading.status) {
      dispatch(chatAction.changeStatus(item));
      dispatch(chatAction.getMessagesList(item.id, userId));
      dispatch(chatAction.getChatInfo(item.id));
    }
  };

  return (
    <div className="chat-list">
      <div className="chat-list-header">
        <span className="h2">Chat</span>
      </div>
      <div className="chat-list-container">
        {listLoading.status && <img className="loader-chat-list" src={Loader} alt="loading ..." />}
        {!listLoading.status && chatsList.length === 0 && <span className="text-item">No posts found</span>}
        {chatsList.map((item) => (
          <div
            key={item.id}
            className={`chat-list-item${item.id === activeChatObject.id ? ' active' : ''}`}
            onClick={() => clickOnChatListItem(item)}
          >
            <div>
              {item.image ? (
                <img className="chat-list-item-initials" src={item.image} alt="avatar" />
              ) : (
                <div className="chat-list-item-initials">{item.name[0] + item.name[item.name.length - 1]}</div>
              )}
            </div>
            <div>
              <div className="chat-list-item-info-container">
                <div className="text-body">{item.name}</div>
                <div className="text-caption">{item.time}</div>
              </div>
              <div>
                <p className="text-small-body message">{item.message}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ChatsList;

import React from 'react';
import ReactHtmlParser from 'react-html-parser';
//types
import { chatMessagesListItemType, chatListItemType } from '../../../../types/acions/chat';

interface MessageProductProps {
  item: chatMessagesListItemType;
  activeChatObject: chatListItemType;
}

const MessageProduct: React.FC<MessageProductProps> = (props: MessageProductProps) => {
  const { item, activeChatObject } = props;
  return (
    <div key={item.id} className={`messages-item ${item.isCustomersMessage ? `customer-message` : ''}`}>
      <div className="messages-item-initials-container">
        {item.isCustomersMessage ? (
          <>
            {activeChatObject.image.length > 0 ? (
              <img className="chat-list-item-initials" src={activeChatObject.image} alt="avatar" />
            ) : (
              <div className="chat-list-item-initials">
                {activeChatObject.name[0] + activeChatObject.name[activeChatObject.name.length - 1]}
              </div>
            )}
          </>
        ) : null}
      </div>
      <div>
        <p className="text-caption messages-item-time">{item.time}</p>
        <div className="messages-item-product">
          <img
            src={
              item.message.pictures.length > 0
                ? item.message.pictures[0].url
                : 'https://tjszkxabrz-flywheel.netdna-ssl.com/wp-content/uploads/2016/05/No-Image.jpg'
            }
            alt="prev-img"
          />
          <span className="text-subhead-small product-title">
            {item.message.rusName ? ReactHtmlParser(item.message.rusName) : ''}
          </span>
          <p className="text-small-body product-description">
            {item.message.releaseForm ? ReactHtmlParser(item.message.releaseForm) : ''}
          </p>
          <span className="product-price">
            {item.message.pharmacyProductsAggregationData && (
              <>
                <span className="text-small-body">от</span>
                <span className="text-subhead-small">{item.message.pharmacyProductsAggregationData.minPrice}р</span>
              </>
            )}
          </span>
        </div>
      </div>
    </div>
  );
};

export default MessageProduct;

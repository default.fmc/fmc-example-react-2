import React, { useState } from 'react';
import Lightbox from 'react-image-lightbox';
// types
import { chatListItemType, chatMessagesListItemType } from '../../../../types/acions/chat';
// style
import 'react-image-lightbox/style.css';

interface MessageAplicationProps {
  item: chatMessagesListItemType;
  activeChatObject: chatListItemType;
}

const MessageAplication: React.FC<MessageAplicationProps> = (props: MessageAplicationProps) => {
  const { item, activeChatObject } = props;

  const [isOpen, setIsOpen] = useState(false);

  return (
    <div key={item.id} className={`messages-item ${item.isCustomersMessage ? `customer-message` : ''}`}>
      <div className="messages-item-initials-container">
        {item.isCustomersMessage ? (
          <>
            {activeChatObject.image.length > 0 ? (
              <img className="chat-list-item-initials" src={activeChatObject.image} alt="avatar" />
            ) : (
              <div className="chat-list-item-initials">
                {activeChatObject.name[0] + activeChatObject.name[activeChatObject.name.length - 1]}
              </div>
            )}
          </>
        ) : null}
      </div>
      <div>
        <p className="text-caption messages-item-time">{item.time}</p>
        <div className="messages-item-text aplication">
          <img src={item.message.url} alt="file" onClick={() => setIsOpen(true)} />
        </div>
      </div>
      {isOpen && <Lightbox mainSrc={item.message.url} onCloseRequest={() => setIsOpen(false)} />}
    </div>
  );
};

export default MessageAplication;

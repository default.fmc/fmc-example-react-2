import React, { useState } from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';
// actions
import chatAction from '../../../../redux/actions/chat';
// types
import { chatListItemType } from '../../../../types/acions/chat';

interface MessageReceptProps {
  item: any;
  activeChatObject: chatListItemType;
}

const MessageRecept: React.FC<MessageReceptProps> = (props: MessageReceptProps) => {
  const { item, activeChatObject } = props;
  const [downloadBtnController, setDownloadBtnController] = useState(false);

  const download = async (uuid) => {
    setDownloadBtnController(true);
    chatAction.dowloadRecipesFromList(uuid, setDownloadBtnController, item);
  };

  return (
    <div key={item.id} className={`messages-item ${item.isCustomersMessage ? `customer-message` : ''}`}>
      <div className="messages-item-initials-container">
        {item.isCustomersMessage ? (
          <>
            {activeChatObject.image.length > 0 ? (
              <img className="chat-list-item-initials" src={activeChatObject.image} alt="avatar" />
            ) : (
              <div className="chat-list-item-initials">
                {activeChatObject.name[0] + activeChatObject.name[activeChatObject.name.length - 1]}
              </div>
            )}
          </>
        ) : null}
      </div>
      <div>
        <p className="text-caption messages-item-time">{item.time}</p>
        <div className="messages-item-text aplication pdf">
          <img
            className="pdf"
            src={'https://help.online.uts.edu.au/wp-content/uploads/2018/06/adobe-pdf-icon.png'}
            alt="file"
          />
          <div className="messages-item-desc">
            <span className="file-title">{item.recipeImage.originalFilename}</span>
            <button
              onClick={() => download(item.recipeImage.uuid)}
              className="text-button"
              disabled={downloadBtnController}
              style={{ position: 'relative' }}
            >
              {downloadBtnController ? (
                <Dimmer active>
                  <Loader size="tiny" />
                </Dimmer>
              ) : (
                'Download'
              )}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MessageRecept;

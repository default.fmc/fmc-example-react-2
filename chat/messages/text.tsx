import React from 'react';
// types
import { chatMessagesListItemType, chatListItemType } from '../../../../types/acions/chat';

interface MessageTextProps {
  item: chatMessagesListItemType;
  activeChatObject: chatListItemType;
}

const MessageText: React.FC<MessageTextProps> = (props: MessageTextProps) => {
  const { item, activeChatObject } = props;
  return (
    <div key={item.id} className={`messages-item ${item.isCustomersMessage ? `customer-message` : ''}`}>
      <div className="messages-item-initials-container">
        {item.isCustomersMessage ? (
          <>
            {activeChatObject.image.length > 0 ? (
              <img className="chat-list-item-initials" src={activeChatObject.image} alt="avatar" />
            ) : (
              <div className="chat-list-item-initials">
                {activeChatObject.name[0] + activeChatObject.name[activeChatObject.name.length - 1]}
              </div>
            )}
          </>
        ) : null}
      </div>
      <div>
        <p className="text-caption messages-item-time">{item.time}</p>
        <div className="messages-item-text">
          <p className="text-small-body">{item.message}</p>
        </div>
      </div>
    </div>
  );
};

export default MessageText;

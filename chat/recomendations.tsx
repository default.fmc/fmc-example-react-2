import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
// components
import InputField from '../../molecules/inputField';
import RecomendationsList from './recomendationsList';
// actions
import chatAction from '../../../redux/actions/chat';
// types
import { chatListItemType } from '../../../types/acions/chat';
import { loadingType } from '../../../types/acions/app';
import { searchedProductType } from '../../../types/acions/product';

interface RecomendationsProps {
  activeChatObject: chatListItemType;
  recomendationResult: Array<searchedProductType>;
  recomendationResultLoading: loadingType;
}

let timerId: any = false;
const Recomendations: React.FC<RecomendationsProps> = (props: RecomendationsProps) => {
  const { activeChatObject, recomendationResult, recomendationResultLoading } = props;

  const [input, setInput] = useState('');
  const [resultController, setResultController] = useState(false);

  const dispatch = useDispatch();

  useEffect(() => {
    setInput('');
    dispatch(chatAction.recomendationResult([]));
  }, [activeChatObject.globalStatus]);

  const getRecomendationResult = (data) => {
    dispatch(chatAction.getRecomendationResult(data));
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setInput(value);

    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      if (value.length === 0) {
        setResultController(false);
        return dispatch(chatAction.recomendationResult([]));
      }
      setResultController(true);
      getRecomendationResult(value);
    }, 500);
  };

  const recomendationPushToChat = (id) => {
    const data = {
      chat_id: activeChatObject.id,
      product_id: id,
    };
    dispatch(chatAction.recomendationPushToChat(data));
  };

  return (
    <div className="recomendations">
      <h1 className="recomendations-heading">Recomendations</h1>
      <div className="recomendations-inner">
        <InputField classes="input-field w-100p" label="Medecine">
          <input
            type="text"
            placeholder="Medecine name"
            value={input}
            onChange={handleChange}
            disabled={activeChatObject.globalStatus === 'closed' || activeChatObject.globalStatus === 'close_request'}
          />
        </InputField>
      </div>
      {activeChatObject.globalStatus !== 'closed' && activeChatObject.globalStatus !== 'close_request' && (
        <>
          {recomendationResultLoading.status && (
            <div className="recomendations-prev-result">
              <span className="text-item">loading ...</span>
            </div>
          )}
          {recomendationResult.length === 0 && recomendationResultLoading.status === false && input.length > 0 && (
            <div className="recomendations-prev-result">
              <span className="text-item">no result</span>
            </div>
          )}
          {resultController && recomendationResult.length > 0 && recomendationResultLoading && (
            <div className="recomendations-result">
              {recomendationResult.map((item) => {
                return (
                  <div className="result-item" key={item.globalProductId}>
                    <div className="result-body">
                      <img
                        src={
                          item.pictures.length > 0
                            ? item.pictures[0].url
                            : 'https://tjszkxabrz-flywheel.netdna-ssl.com/wp-content/uploads/2016/05/No-Image.jpg'
                        }
                        alt="prev img"
                      />
                      <div className="result-description">
                        <div className="column">
                          <span className="text-item">ID: {item.globalProductId}</span>

                          <span className="text-item">Name: {ReactHtmlParser(item.rusName)}</span>
                        </div>
                        <div className="column">
                          <span className="text-item">Category (ATC): {item.categoryAtcCode}</span>
                        </div>
                      </div>
                    </div>
                    <button
                      className="btn btn-default text-item"
                      onClick={() => {
                        setResultController(false);
                        recomendationPushToChat(item.globalProductId);
                      }}
                    >
                      Use this
                    </button>
                  </div>
                );
              })}
            </div>
          )}

          {null && <RecomendationsList />}
        </>
      )}
    </div>
  );
};

export default Recomendations;

import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'semantic-ui-react';
// components
import InputField from '../../molecules/inputField';
import DropzoneModal from '../../atoms/modals/dropzoneModal';
// actions
import chatAction from '../../../redux/actions/chat';
// types
import { chatListItemType } from '../../../types/acions/chat';
import { loadingType } from '../../../types/acions/app';

interface MessageInputProps {
  disabled: boolean;
  activeChatObject: chatListItemType;
  messagesLoading: loadingType;
}

const MessageInput: React.FC<MessageInputProps> = (props: MessageInputProps) => {
  const { disabled, activeChatObject, messagesLoading } = props;

  const [message, setMessage] = useState('');
  const [modalOpen, setModalOpen] = useState(false);

  const dispatch = useDispatch();

  const handleChange = (e) => {
    setMessage(e.target.value);
  };

  const handlerDropzoneModal = () => {
    setModalOpen(!modalOpen);
  };

  const data = {
    id: activeChatObject.id,
    text: message,
  };

  const handleSubmit = (e) => {
    if (!disabled) {
      e.preventDefault();
      message.length > 0 && dispatch(chatAction.sendMessage(data));
      setMessage('');
    }
  };

  const saveAction = (newFiles, stateImages) => {
    const data = {
      id: activeChatObject.id,
      newFiles: newFiles,
      stateImages: stateImages,
    };
    dispatch(chatAction.sendImageMessage(data));
  };

  return (
    <>
      <Modal open={modalOpen} onClose={handlerDropzoneModal} handlerDropzoneModal={handlerDropzoneModal}>
        <div className="dzu icon icon-modal-close" onClick={() => setModalOpen(false)} />
        <DropzoneModal saveAction={saveAction} handlerDropzoneModal={handlerDropzoneModal} />
      </Modal>
      <div className={activeChatObject.globalStatus !== 'closed' ? 'chat-input' : 'chat-input center'}>
        {activeChatObject.globalStatus !== 'closed' && !messagesLoading.status ? (
          <form onSubmit={handleSubmit}>
            <InputField
              classes="input-field"
              icon={`icon-field-add-file${disabled ? ' disabled' : ''}`}
              iconAction={!disabled ? handlerDropzoneModal : null}
            >
              <input type="text" onChange={handleChange} value={message} disabled={disabled} />
            </InputField>
            <button disabled={disabled}>
              <div className={`icon icon-chat-submit${disabled ? ' disabled' : ''}`}></div>
            </button>
          </form>
        ) : (
          !messagesLoading.status && <span className="text-item">Chat is closed</span>
        )}
      </div>
    </>
  );
};

export default MessageInput;

import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'semantic-ui-react';
// components
import AddNewRecipeModal from '../modals/addNewRecipeModal';
// actions
import chatAction from '../../../redux/actions/chat';
// types
import { stateType } from '../../../types/acions/state';
import { chatListItemType } from '../../../types/acions/chat';

const ButtonsBlock: FC = () => {
  const [modalOpen, setModalOpen] = useState(false);

  const activeChatObject: chatListItemType = useSelector((state: stateType) => state.chat.activeChatObject)
  const userId: number = useSelector((state: stateType) => state.user.id)
  const userType: string = useSelector((state: stateType) => state.user.userType)

  const dispatch = useDispatch();

  const data = {
    uuid: userId,
    page: 1,
    per_page: 100,
  };

  const handlerModal = () => {
    setModalOpen(false);
  };

  return (
    <div className="buttons-container">
      {userType === 'doctor' && (
        <Modal
          size="tiny"
          open={modalOpen}
          onClose={handlerModal}
          trigger={
            <button
              onClick={() => setModalOpen(true)}
              className="button btn-default white"
              disabled={activeChatObject.globalStatus === 'closed' || activeChatObject.globalStatus === 'close_request'}
            >
              Add a recipe
            </button>
          }
        >
          <div className="icon icon-modal-close" onClick={() => setModalOpen(false)} />
          <AddNewRecipeModal
            handlerModal={handlerModal}
            customer={activeChatObject.customer}
            activeChatId={activeChatObject.id}
            action={() => {
              return true;
            }}
            reciever={'Reciever'}
          />
        </Modal>
      )}

      <button
        className="button btn-default"
        disabled={activeChatObject.globalStatus === 'closed' || activeChatObject.globalStatus === 'close_request'}
        onClick={() => dispatch(chatAction.closeChat(activeChatObject.id, data))}
      >
        End chat
      </button>
    </div>
  );
};


export default ButtonsBlock;

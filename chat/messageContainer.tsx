import React, { FC, useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Element, scroller } from 'react-scroll';
import _ from 'lodash';
// components
import MessageInput from './messageInput';
import MessageText from './messages/text';
import MessageAplication from './messages/aplication';
import MessageProduct from './messages/product';
import MessageRecept from './messages/recept';
// types
import { stateType } from '../../../types/acions/state';
import { chatListItemType, chatMessagesListItemType, messageType } from '../../../types/acions/chat';
import { loadingType } from '../../../types/acions/app';
// images
import Loader from '../../../assets/image/loaders/loading.gif';

interface IMessageContainer {
  messagesList: Array<chatMessagesListItemType>;
  messagesLoading: loadingType;
  activeChatObject: chatListItemType;
}

const MessageContainer: FC = () => {

  const { 
    messagesLoading,
    activeChatObject, 
    messagesList }: IMessageContainer = useSelector((state: stateType) => state.chat)

  const refMessages = useRef();

  useEffect(() => {
    scrollToBottom();
  }, []);

  const scrollToBottom = () => {
    scroller.scrollTo('containerWrapperElement', {
      containerId: 'containerWrapperElement',
      delay: 0,
      smooth: true,
      duration: 0,
      offset: 9999,
    });
  };

  useEffect(() => {
    scrollToBottom();
  }, [messagesList]);

  const messagesController = (item: messageType) => {
    if (item) {
      switch (item.type) {
        case 'text':
          return <MessageText key={item.id} item={item} activeChatObject={activeChatObject} />;
        case 'file':
          return <MessageAplication key={item.id} item={item} activeChatObject={activeChatObject} />;
        case 'product':
          return <MessageProduct key={item.id} item={item} activeChatObject={activeChatObject} />;
        case 'recipt':
          return <MessageRecept key={item.id} item={item} activeChatObject={activeChatObject} />;
      }
    }
  };

  return (
    <div className="messages" ref={refMessages}>
      <Element
        name="containerWrapperElement"
        id="containerWrapperElement"
        className={messagesLoading.status ? 'messages-container loading' : 'messages-container'}
      >
        {messagesLoading.status && <img className="loader-chat-list" src={Loader} alt="loading ..." />}
        {!messagesLoading.status && messagesList.map((item) => messagesController(item))}
      </Element>
      {!_.isEmpty(activeChatObject) && (
        <MessageInput
          disabled={messagesLoading.status || activeChatObject.globalStatus === 'close_request'}
          messagesLoading={messagesLoading}
          activeChatObject={activeChatObject}
        />
      )}
    </div>
  );
};

export default MessageContainer;

import React, { FC } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import _ from 'lodash';
// components
import ChatsList from './chatsList';
import MessageContainer from './messageContainer';
// types
import { stateType } from '../../../types/acions/state';
import { chatListItemType } from '../../../types/acions/chat';

const Chat: FC = () => {

  const activeChatObject: chatListItemType = useSelector((state: stateType) => state.chat.activeChatObject, shallowEqual)
  return (
    <div className="chat">
      <ChatsList />
      {!_.isEmpty(activeChatObject) && <MessageContainer />}
    </div>
  );
};

export default Chat;
